import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { SearchBar, Header, CheckBox } from 'react-native-elements';
import { Icon } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import { ScrollView } from 'react-native-gesture-handler';

export default class Home extends React.Component {
  render() {
    return (
      <View>
        <ScrollView>
          <Header
            leftComponent={{ icon: 'menu', color: '#fff' }}
            centerComponent={{ text: 'Software Services', style: { color: '#fff' } }}
            rightComponent={{ icon: 'home', color: '#fff' }}
          />
          <View style={{ padding: 10, marginTop: 10 }}>
            <Button
              title="Add Family Member"
              onPress={() =>
                this.props.navigation.navigate('Addfamilymember')
              }
            />
          </View>

          <View style={{ padding: 10, marginTop: 10 }}>
            <Button
              title="Location"
              onPress={() =>
                this.props.navigation.navigate('Orders')
              }
            />
          </View>
          <View style={{ padding: 10, marginTop: 10 }}>
            <Button
              title="OrderHistory"
              onPress={() =>
                this.props.navigation.navigate('OrderHistory')
              }
            />
          </View>
          <View style={{ padding: 10, marginTop: 10 }}>
            <Button
              title="My Cart"
              onPress={() =>
                this.props.navigation.navigate('MyCart')
              }
            />
          </View>
          <View style={{ padding: 10, marginTop: 10 }}>
            <Button
              title="Prescription"
              onPress={() =>
                this.props.navigation.navigate('Prescription')
              }
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});