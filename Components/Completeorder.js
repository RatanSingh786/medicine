import React from 'react';
import { Text, View, StyleSheet, TextInput, FlatList, Keyboard, Button, Image, ActivityIndicator, Platform, SafeAreaView } from 'react-native';
import { SearchBar, Header, CheckBox } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import { Icon } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import ModalDropdown from 'react-native-modal-dropdown';

export default class Completeorder extends React.Component {
  constructor(props) {
    super(props);
  }
  handleCoupanCode = (text) => {
    this.setState({ password: text })
  }
  
  render() {
    // let data = [{
    //   value: 'Marcos',
    // }, {
    //   value: 'Daniel',
    // }, {
    //   value: 'Peater',
    // }];
    return (
      <View>
        <ScrollView>
          <View style={styles.medicinelisting}>
            <Text style={{ fontSize: 30, fontWeight: '600', borderBottomWidth: 1, borderColor: 'black' }}>Complete Your Order</Text>
          </View>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={styles.container}>
          <View style={{ marginRight: 10,borderRadius: 150 / 2,overflow: "hidden",borderWidth: 3,borderColor: "blue",width:80 }}><Button title="4 Oct" /></View>
            <View style={{ marginRight: 10}}><Text style={{borderRadius: 50,overflow: "hidden",borderWidth: 3,borderColor: "#ccc",height:40,width:80,textAlign:'center',paddingTop:10}}>5 Oct</Text></View>
            <View style={{ marginRight: 10}}><Text style={{borderRadius: 50,overflow: "hidden",borderWidth: 3,borderColor: "#ccc",height:40,width:80,textAlign:'center',paddingTop:10}}>6 Oct</Text></View>
            <View style={{ marginRight: 10}}><Text style={{borderRadius: 50,overflow: "hidden",borderWidth: 3,borderColor: "#ccc",height:40,width:80,textAlign:'center',paddingTop:10}}>7 Oct</Text></View>
            <View style={{ marginRight: 10}}><Text style={{borderRadius: 50,overflow: "hidden",borderWidth: 3,borderColor: "#ccc",height:40,width:80,textAlign:'center',paddingTop:10}}>8 Oct</Text></View>
            <View style={{ marginRight: 10}}><Text style={{borderRadius: 50,overflow: "hidden",borderWidth: 3,borderColor: "#ccc",height:40,width:80,textAlign:'center',paddingTop:10}}>9 Oct</Text></View>
            <View style={{ marginRight: 10}}><Text style={{borderRadius: 50,overflow: "hidden",borderWidth: 3,borderColor: "#ccc",height:40,width:80,textAlign:'center',paddingTop:10}}>10 Oct</Text></View>
            <View style={{ marginRight: 10}}><Text style={{borderRadius: 50,overflow: "hidden",borderWidth: 3,borderColor: "#ccc",height:40,width:80,textAlign:'center',paddingTop:10}}>11 Oct</Text></View>
          </ScrollView>

          <View style={styles.medicinelisting}>
            <View style={{ position: 'absolute', left: 20 }}>
              <Image source={require('./img/16.png')} style={{ width: 40, height: 40 }} /></View>
            <View style={{ position: 'absolute', right: 20, width: 250 }}>
            <ModalDropdown options={['Michele', 'Marcos']} style={{ borderBottomWidth: 1, borderColor: 'black' }}/>
            </View>
          </View>
          <View style={styles.medicinelisting}>
            <View style={{ position: 'absolute', left: 20 }}>
              <Image source={require('./img/17.png')} style={{ width: 40, height: 40 }} /></View>
            <TextInput style={{ position: 'absolute', right: 20, width: 250,borderBottomWidth: 1, borderColor: 'black' }}
              underlineColorAndroid="transparent"
              placeholder="9999999999"
              placeholderTextColor="#ccc"
              onChangeText={this.handleCoupanCode} />
          </View>
          <View style={styles.medicinelisting}>
            <View style={{ position: 'absolute', left: 20 }}>
              <Image source={require('./img/18.png')} style={{ width: 40, height: 40 }} /></View>
            <TextInput style={{ position: 'absolute', right: 20, width: 250,borderBottomWidth: 1, borderColor: 'black' }}
              underlineColorAndroid="transparent"
              placeholder="Flat No, Apartment,Area,Landmark"
              placeholderTextColor="#ccc"
              onChangeText={this.handleCoupanCode} />
          </View>
          <View style={styles.medicinelisting}>
            <TextInput
              style={{ position: 'absolute', left: 130,borderBottomWidth: 1, borderColor: 'black' }}
              underlineColorAndroid="transparent"
              placeholder="28200x"
              placeholderTextColor="#ccc"
              onChangeText={this.handleCoupanCode} />
            <TextInput
              style={{ position: 'absolute', right: 20,borderBottomWidth: 1, borderColor: 'black' }}
              underlineColorAndroid="transparent"
              placeholder="Agra Uttar Pradesh"
              placeholderTextColor="#ccc"
              onChangeText={this.handleCoupanCode} />
          </View>
          <View style={styles.medicinelisting}>
            <View style={{ position: 'absolute', left: 20,top:20 }}>
              <Text>Choose Time Slot</Text>
            </View>
            <TextInput style={{ position: 'absolute', right: 20, width: 200,borderBottomWidth: 1, borderColor: 'black' }}
              underlineColorAndroid="transparent"
              placeholder="9999999999"
              placeholderTextColor="#ccc"
              onChangeText={this.handleCoupanCode} />
          </View>
          <View>
            <ScrollView horizontal={true} style={styles.container}>
            <View style={{ marginRight: 10,borderRadius: 150 / 2,overflow: "hidden",borderWidth: 3,borderColor: "blue",width:130 }}><Button title="10AM-12AM" /></View>
            <View style={{ marginRight: 10}}><Text style={{borderRadius: 50,overflow: "hidden",borderWidth: 3,borderColor: "#ccc",height:40,width:130,textAlign:'center',paddingTop:10}}>02PM-4PM</Text></View>
            <View style={{ marginRight: 10}}><Text style={{borderRadius: 50,overflow: "hidden",borderWidth: 3,borderColor: "#ccc",height:40,width:130,textAlign:'center',paddingTop:10}}>04PM-6PM</Text></View>
            <View style={{ marginRight: 10}}><Text style={{borderRadius: 50,overflow: "hidden",borderWidth: 3,borderColor: "#ccc",height:40,width:130,textAlign:'center',paddingTop:10}}>06PM-8PM</Text></View>
            <View style={{ marginRight: 10}}><Text style={{borderRadius: 50,overflow: "hidden",borderWidth: 3,borderColor: "#ccc",height:40,width:130,textAlign:'center',paddingTop:10}}>08PM-10PM</Text></View>
            </ScrollView>
          </View>
          <View style={{padding: 25, marginTop: 10, marginBottom: 0, marginRight: 10, marginLeft: 10,backgroundColor:'#a9a9a9'}}>
            <Text>By Booking this you agree to the TC</Text>
            <Button
              title="Confirm Booking"
              onPress={() =>
                this.props.navigation.navigate('Home')
              }
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  medicinelisting: {
    padding: 25, marginTop: 10, marginBottom: 0, marginRight: 10, marginLeft: 10
  },
  container: {
    flex: 1,
    padding: 10,
  },
});
