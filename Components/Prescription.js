import React from 'react';
import { Text, View, StyleSheet, TextInput, FlatList, Keyboard, Button, Image, ActivityIndicator, Platform, SafeAreaView } from 'react-native';
import { SearchBar, Header, CheckBox } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import { Icon } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import ModalDropdown from 'react-native-modal-dropdown';
import ImagePicker from 'react-native-image-picker';

export default class Prescription extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filePath: {},
        };
    }
    chooseFile = () => {
        var options = {
            title: 'Select Image',
            customButtons: [
                { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
            ],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
            } else {
                let source = response;
                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };
                this.setState({
                    filePath: source,
                });
            }
        });
    };
    render() {
        return (
            <View>
                <ScrollView>
                    <View style={styles.medicinelisting}>
                        <Image source={require('./img/2.png')} style={{ left: 20, width: 50, height: 50 }} />
                        <Text style={{ position: 'absolute', right: 130, top: 40 }}>Order With Prescription</Text>
                    </View>
                    <View style={styles.medicinelisting}>
                        <Image source={require('./img/2.png')} style={{ left: 20, width: 50, height: 50 }} />
                        <Text style={{ position: 'absolute', right: 110, top: 40 }}>Order Without Prescription</Text>
                    </View>
                    <View style={styles.container}>
                        <View style={styles.container}>
                            <Image
                                source={{
                                    uri: 'data:image/jpeg;base64,' + this.state.filePath.data,
                                }}
                                style={{ width: 100, height: 100 }}
                            />
                            <Image
                                source={{ uri: this.state.filePath.uri }}
                                style={{ width: 250, height: 250 }}
                            />
                            <Text style={{ alignItems: 'center' }}>
                                {this.state.filePath.uri}
                            </Text>
                            <Button title="Choose File" onPress={this.chooseFile.bind(this)} />
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    medicinelisting: {
        padding: 25, marginTop: 10, marginBottom: 0,
        marginRight: 10, marginLeft: 10,
        backgroundColor:'#fff'
    },
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
