import React from 'react';
import { Text, View, StyleSheet, TextInput, FlatList, Keyboard, Button, Image, ActivityIndicator, Platform, SafeAreaView } from 'react-native';
import { SearchBar, Header, CheckBox } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import { Icon } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import ModalDropdown from 'react-native-modal-dropdown';
import ImageSlider from 'react-native-image-slider';

export default class Orders extends React.Component {
    render() {
        const images = [
          'https://placeimg.com/640/640/nature',
          'https://placeimg.com/640/640/people',
          require('./img/6.png'),
          require('./img/7.png'),
          require('./img/2.png'), 
        ];
        return (
            <View>
                <ScrollView>
                    <View>
                        <View style={{height:250,width:450}}><ImageSlider images={images} /></View>
                    </View>
                    <View style={styles.medicinelisting}>
                        <Image source={require('./img/2.png')} style={{left:20,width:50,height:50}} />
                        <Text style={{position:'absolute',right:130,top:40}}>Order With Prescription</Text>
                    </View>
                    <View style={styles.medicinelisting}>
                        <Image source={require('./img/2.png')} style={{left:20,width:50,height:50}} />
                        <Text style={{position:'absolute',right:110,top:40}}>Order Without Prescription</Text>
                    </View>
                    <View style={styles.medicinelisting}>
                    <Button
                    title="Order History"
                    onPress={() =>
                        this.props.navigation.navigate('OrderHistory')
                    }
                    />
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    medicinelisting: {
        padding: 25, marginTop: 10, marginBottom: 0,
        marginRight: 10, marginLeft: 10,
        backgroundColor:'#fff'
    },
    container: {
        flex: 1,
        padding: 10,
    },
});
