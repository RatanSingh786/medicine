import React from 'react';
import { Text, View, StyleSheet, TextInput, FlatList, Keyboard, Button, Image, ActivityIndicator, Platform, SafeAreaView } from 'react-native';
import { SearchBar, Header, CheckBox } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import { Icon } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import ModalDropdown from 'react-native-modal-dropdown';

export default class Ordersuccess extends React.Component {
    constructor(props) {
        super(props);
    }
    handleCoupanCode = (text) => {
        this.setState({ password: text })
    }

    render() {
        // let data = [{
        //   value: 'Marcos',
        // }, {
        //   value: 'Daniel',
        // }, {
        //   value: 'Peater',
        // }];
        return (
            <View>
                <ScrollView>
                    <View style={styles.medicinelisting}>
                        <View style={{marginLeft:60}}>
                            <Image source={require('./img/21.png')} style={{width:200,height:200}} />
                        </View>
                    </View>
                    <View style={styles.medicinelisting}>
                        <Text style={{ fontSize: 15, fontWeight: '400',textAlign:'center'}}>Thankyou For Ordering With Us.</Text>
                    </View>
                    <View style={styles.medicinelisting}>
                        <Image source={require('./img/20.png')} style={{left:20,width:50,height:50}} />
                        <Text style={{position:'absolute',right:80,top:40}}>We will Get Back To you In 5 Min</Text>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    medicinelisting: {
        padding: 25, marginTop: 10, marginBottom: 0, 
        marginRight: 10, marginLeft: 10,
        shadowColor:'gray',
    },
    container: {
        flex: 1,
        padding: 10,
    },
});
