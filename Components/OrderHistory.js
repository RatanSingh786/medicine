import React from 'react';
import { Text, View, StyleSheet, TextInput, FlatList, Keyboard, Button, Image, ActivityIndicator, Platform, SafeAreaView } from 'react-native';
import { SearchBar, Header, CheckBox } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import { Icon } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
export default class OrderHistory extends React.Component {
  constructor(props) {
    super(props);
    //setting default state
    this.state = { isLoading: true, search: '' };
    this.arrayholder = [];
  }
  increment() {
    this.setState({
      count: this.state.count + 1
    });
  };

  componentDidMount() {
    return fetch('https://jsonplaceholder.typicode.com/posts')
      .then(response => response.json())
      .then(responseJson => {
        this.setState(
          {
            isLoading: false,
            dataSource: responseJson,
          },
          function () {
            this.arrayholder = responseJson;
          }
        );
      })
      .catch(error => {
        console.error(error);
      });
  }
  search = text => {
    console.log(text);
  };
  clear = () => {
    this.search.clear();
  };
  SearchFilterFunction(text) {
    const newData = this.arrayholder.filter(function (item) {
      const itemData = item.title ? item.title.toUpperCase() : ''.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      dataSource: newData,
      search: text,
    });
  }
  ListViewItemSeparator = () => {
    return (
      <View
        style={{
          height: 0.3,
          width: '90%',
          backgroundColor: '#080808',
        }}
      />
    );
  };
  handleCoupanCode = (text) => {
    this.setState({ password: text })
  }


  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, paddingTop: 0 }}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
      <View style={{flex:1}} >
        <ScrollView>
        
          <View style={styles.orderListing} >
          <Text style={{ textAlign: 'center',fontWeight: 'bold',fontSize: 15,}} onPress={() =>
        this.props.navigation.navigate('OrderHistoryDetails', { name: 'OrderHistoryDetails' })
      }>Order no. #95481</Text>
          
          <View style={{marginTop:5}}>
              <Text>Patient Name</Text>
  
            </View>
            <View>
              <Text>Order Time</Text>
              <Text style={{ position: "absolute", left: 80 }}>:</Text>
              <Text style={{ position: "absolute", left: 90 }}>15:30</Text>
              <Text style={{ position: "absolute", right: 30 ,fontSize: 14,paddingBottom:10}}>Status</Text>

            </View>
            <View>
              <Text>Amount   ₹</Text>
              <Text style={{ position: "absolute", left: 80 }}>:</Text>
              <Text style={{ position: "absolute", left: 90 }}>300/-</Text>
              <Text style={{ position: "absolute", right: 15 , backgroundColor: '#F6a90F', width: 70, height: 18, paddingLeft: 20, paddingTop: 20, borderRadius: 20 ,color: '#ffffff'}}>Pending</Text>
              <Text style={{ position: "absolute", right: 27 ,fontSize: 12,color:'#FFFFFF'}}>Pending</Text>
            </View>
            <View>
              <Text>ETA</Text>
              <Text style={{ position: "absolute", left: 80 }}>:</Text>
              <Text style={{ position: "absolute", left: 90 }}>40Min</Text>
            </View>
            <View>
              <Text>Mode</Text>
              <Text style={{ position: "absolute", left: 80 }}>:</Text>
              <Text style={{ position: "absolute", left: 90 }}>Normal Delivery</Text>
            </View>
           
            <View style={{width: '100%',
      justifyContent: 'center',
      alignItems: 'center'}}>
            <Text></Text>
              <Text style={{ backgroundColor: '#F6a90F', width: 60, height: 30, paddingLeft: 20, paddingTop: 25, borderRadius: 20 ,color: '#ffffff'}}>Edit</Text>
              <Text style={{  position: "absolute",fontSize: 10,color:'#ffffff',paddingTop:18,fontWeight: 'bold'}}>Edit</Text>

              <Text style={{ position: "absolute", right: 15 , backgroundColor: 'red', width: 60, height: 30, paddingLeft: 20,top:19,paddingTop: 25, borderRadius: 20 ,color: '#ffffff'}}>Pending</Text>
              <Text style={{ position: "absolute", right: 29 ,fontSize: 10,color:'#FFFFFF',paddingTop:7,fontWeight: 'bold',top:19}}>Reject</Text>

              <Text style={{ position: "absolute", left: 15 , backgroundColor: '#4cbb17', width: 60, height: 30, top:19,paddingLeft: 20, paddingTop: 25, borderRadius: 20 ,color: '#ffffff'}}>Accept</Text>
              <Text style={{ position: "absolute", left: 29 ,fontSize: 10,color:'#FFFFFF',paddingTop:7,fontWeight: 'bold',top:19}}>Accept</Text>
              
            </View>
          
          
          </View>
          
        </ScrollView>
        <View style={styles.talkBubble}>
          
        <View style={styles.talkBubbleSquare} >

        <Text style={{  textAlign: 'center',fontSize: 23,color:'#ffffff',paddingTop:15,fontWeight:"bold"}}>CLOSE</Text>

        </View>
      </View>   
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewStyle: {
    justifyContent: 'center',
    flex: 1,
    backgroundColor: 'gray',
    marginTop: Platform.OS == 'ios' ? 0 : 0
  },
  textStyle: {
    padding: 0,
  },
  medicinelisting: {
    padding: 25, backgroundColor: '#fff', marginTop: 10, marginBottom: 0, marginRight: 10, marginLeft: 10
  },
  orderListing: {
    paddingLeft: 25,paddingRight: 25,paddingBottom: 20,paddingTop: 5, backgroundColor: '#fff', marginTop: 10, marginBottom: 0, marginRight: 10, marginLeft: 10
  },
  CoupanCode: {
    paddingLeft: 20, paddingRight: 20, backgroundColor: '#fff', marginTop: 20
  },
  NextButtonStyle: {
    height: 50,
    padding: 10,
    marginTop: 50
  },
  input: {
    borderBottomWidth: 1, borderColor: 'black'
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 30,
    margin: 2,
    borderColor: '#2a4944',
    borderWidth: 1,
    backgroundColor: '#d2f7f1'
  },talkBubble: {
    backgroundColor: 'transparent'
  },
  talkBubbleSquare: {
    width: '90%',
    height: 60,
    marginLeft:20,
    marginRight:20,
    backgroundColor: '#02386e',
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15

  }
});
