import React from 'react';
import { Text, View, StyleSheet, TextInput, FlatList, Keyboard, Button, Image, ActivityIndicator, Platform, SafeAreaView } from 'react-native';
import { SearchBar, Header, CheckBox } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import { Icon } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
export default class OrderHistoryDetails extends React.Component {
  constructor(props) {
    super(props);
    //setting default state
    this.state = { isLoading: true, search: '' };
    this.arrayholder = [];
  }
  increment() {
    this.setState({
      count: this.state.count + 1
    });
  };

  componentDidMount() {
    return fetch('https://jsonplaceholder.typicode.com/posts')
      .then(response => response.json())
      .then(responseJson => {
        this.setState(
          {
            isLoading: false,
            dataSource: responseJson,
          },
          function () {
            this.arrayholder = responseJson;
          }
        );
      })
      .catch(error => {
        console.error(error);
      });
  }
  search = text => {
    console.log(text);
  };
  clear = () => {
    this.search.clear();
  };
  SearchFilterFunction(text) {
    const newData = this.arrayholder.filter(function (item) {
      const itemData = item.title ? item.title.toUpperCase() : ''.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      dataSource: newData,
      search: text,
    });
  }
  ListViewItemSeparator = () => {
    return (
      <View
        style={{
          height: 0.3,
          width: '90%',
          backgroundColor: '#080808',
        }}
      />
    );
  };
  handleCoupanCode = (text) => {
    this.setState({ password: text })
  }


  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, paddingTop: 0 }}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
      <View style={{flex:1}}>
      
        <ScrollView>
          {/* <SearchBar
            style={{ width: 600 }}
            round
            searchIcon={{ size: 24 }}
            onChangeText={text => this.SearchFilterFunction(text)}
            onClear={text => this.SearchFilterFunction('')}
            placeholder="Type Here..."
            value={this.state.search}
          /> */}
          <View style={styles.talkBubble}>
          
          <View style={styles.talkBubbleSquare} >
  
          <Text style={{  textAlign: 'center',fontSize: 23,color:'#ffffff',paddingTop:15,fontWeight:"bold"}}>Order No. #9548</Text>
  
          </View>
          </View>
          <View style={styles.medicinelisting}>
            <Text style={{ position: "absolute", left: 20 ,marginTop:15}}>Medicine 1</Text>
            <View style={{ position: "absolute", left: 160,marginTop:15 }}><Icon name='add-circle' /></View>
            <Text style={{ position: "absolute", left: 200 ,marginTop:15}}>5</Text>
            <View style={{ position: "absolute", right: 150 ,marginTop:15}}><Icon name="remove-circle" /></View>
            <Text style={{ position: "absolute", right: 20 ,marginTop:15}} >₹100/-</Text>
          </View>
          <View style={styles.medicinelisting}>
            <Text style={{ position: "absolute", left: 20 ,marginTop:15}}>Medicine 2</Text>
            <View style={{ position: "absolute", left: 160,marginTop:15 }}><Icon name='add-circle' /></View>
            <Text style={{ position: "absolute", left: 200 ,marginTop:15}}>5</Text>
            <View style={{ position: "absolute", right: 150 ,marginTop:15}}><Icon name="remove-circle" /></View>
            <Text style={{ position: "absolute", right: 20 ,marginTop:15}} >₹100/-</Text>
          </View>
          <View style={styles.medicinelisting}>
            <Text style={{ position: "absolute", left: 20 ,marginTop:15}}>Medicine3</Text>
            <View style={{ position: "absolute", left: 160,marginTop:15 }}><Icon name='add-circle' /></View>
            <Text style={{ position: "absolute", left: 200 ,marginTop:15}}>5</Text>
            <View style={{ position: "absolute", right: 150 ,marginTop:15}}><Icon name="remove-circle" /></View>
            <Text style={{ position: "absolute", right: 20 ,marginTop:15}} >₹100/-</Text>
          </View>
         
          <View style={styles.amountlisting}>
            <Text style={{ position: "absolute", left: 20, marginTop:15,backgroundColor: '#02386e', width: 100, height: 25, paddingLeft: 25, paddingTop: 20, borderRadius: 20 }}></Text>
            <Text style={{ position: "absolute", left: 28, marginTop:17,color:'#ffffff' }}>XYZ COUPON</Text>
            <Text style={{ position: "absolute", left: 140, color: 'green' ,marginTop:15}}>90% Off</Text>
            <Text style={{ position: "absolute", right: 70, color: 'green' ,marginTop:15}}>-₹30</Text>
            <View style={{ position: "absolute", right: 10, paddingRight: 20 ,marginTop:15 }} ><Icon name='close' /></View>
            <TextInput style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Coupon Code"
              placeholderTextColor="#222222"
              onChangeText={this.handleCoupanCode} />
            <View style={{ width: 100,marginTop:15 , position: "absolute", right: 20, height: 10, top: 25 }}>
              <Button title="APPLY" />
            </View>
          </View>
          <View style={styles.amountlisting}>
            <View>
              <Text>Total Amount(MRP)</Text>
              <Text style={{ position: "absolute", right: 0 }}>₹330</Text>
            </View>
            <View>
              <Text>Coupan Discount</Text>
              <Text style={{ position: "absolute", right: 0, color: 'green' }}>₹30</Text></View>
            <View>
              <Text>Delivery Charge</Text>
              <Text style={{ position: "absolute", right: 0 }}>₹20</Text></View>
          </View>
          {/* <View style={{ marginRight: 10, marginLeft: 10, marginTop: 30 }}>
            <Text style={{ borderBottomWidth: 1, borderColor: 'black' }}>Add Instruction For Order</Text>
          </View> */}

          {/* <View style={styles.amountlisting}>
            <View style={{}} ><Image source={require('./img/7.png')} /></View>
            <Text style={{ position: "absolute", right: 100 }}>1 Prescription Uploaded </Text>
            <View style={{ position: "absolute", right: 10, paddingRight: 20 }} ><Icon name='close' /></View>
          </View>

          <View style={{ marginTop: 10, padding: 10, backgroundColor: 'red' }}>
             <CheckBox
              label='checkbox'
              checked={this.state.checked}
            />
            <View style={{ position: "absolute", left: 80,width:60 }}>
              <Image source={require('./img/13.png')} /></View>
            <Text style={{ position: "absolute", right: 100, color: '#fff',fontWeight:'bold',fontSize:20 }}>Express Delivery</Text>
            <View style={{ position: "absolute", right: 10, paddingRight: 20 }} ><Icon name='close' /></View>
          </View> */}

         
        </ScrollView>
        <View style={{backgroundColor:"#ffffff",padding:20}}>
            <Text>Total Amount</Text>
            <Text style={{fontSize:20}}>₹300</Text>
            <Text style={{color:'green',fontSize:12}}>You save ₹30 </Text>
            <View style={{position:"absolute", right: 0,top:30,width:100}}>
            <Text style={{ position: "absolute", right: 20, marginTop:0,backgroundColor: '#02386e', width: 130, height: 40, paddingLeft: 25, paddingTop: 20, borderRadius: 5 }}></Text>
            <Text style={{ position: "absolute", right: 65, marginTop:9,color:'#ffffff',fontWeight:"bold" ,fontSize:15}}>DONE</Text>
            {/* <Button
              title="NEXT"
              onPress={() =>
                this.props.navigation.navigate('Confirmbooking')
              }
            /> */}
            </View>
          </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewStyle: {
    justifyContent: 'center',
    flex: 1,
    backgroundColor: 'gray',
    marginTop: Platform.OS == 'ios' ? 0 : 0
  },
  textStyle: {
    padding: 0,
  },
  medicinelisting: {
    padding: 25, backgroundColor: '#fff', marginTop: 2, marginBottom: 0, marginRight: 10, marginLeft: 10
  },
  amountlisting: {
    padding: 15, backgroundColor: '#fff', marginTop: 15      , marginBottom: 0, marginRight: 10, marginLeft: 10
  },
  CoupanCode: {
    paddingLeft: 20, paddingRight: 20, backgroundColor: '#fff', marginTop: 20
  },
  NextButtonStyle: {
    height: 50,
    padding: 10,
    marginTop: 50
  },
  input: {
    borderBottomWidth: 1, borderColor: 'black',marginTop:20,fontSize:16
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 30,
    margin: 2,
    borderColor: '#2a4944',
    borderWidth: 1,
    backgroundColor: '#d2f7f1'
  },
  talkBubble: {
    backgroundColor: 'transparent'
  },
  talkBubbleSquare: {
    width: '100%',
    height: 60,
    backgroundColor: '#02386e',
    borderBottomRightRadius: 15,
    borderBottomLeftRadius: 15,
    marginBottom:10
  }
});
