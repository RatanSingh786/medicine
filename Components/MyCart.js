import React from 'react';
import { Text, View, StyleSheet, TextInput, FlatList, Keyboard, Button, Image, ActivityIndicator, Platform, SafeAreaView } from 'react-native';
import { SearchBar, Header, CheckBox } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import { Icon } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
export default class MyCart extends React.Component {
  constructor(props) {
    super(props);
    //setting default state
    this.state = { isLoading: true, search: '' };
    this.arrayholder = [];
  }
  increment() {
    this.setState({
      count: this.state.count + 1
    });
  };

  componentDidMount() {
    return fetch('https://jsonplaceholder.typicode.com/posts')
      .then(response => response.json())
      .then(responseJson => {
        this.setState(
          {
            isLoading: false,
            dataSource: responseJson,
          },
          function () {
            this.arrayholder = responseJson;
          }
        );
      })
      .catch(error => {
        console.error(error);
      });
  }
  search = text => {
    console.log(text);
  };
  clear = () => {
    this.search.clear();
  };
  SearchFilterFunction(text) {
    const newData = this.arrayholder.filter(function (item) {
      const itemData = item.title ? item.title.toUpperCase() : ''.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      dataSource: newData,
      search: text,
    });
  }
  ListViewItemSeparator = () => {
    return (
      <View
        style={{
          height: 0.3,
          width: '90%',
          backgroundColor: '#080808',
        }}
      />
    );
  };
  handleCoupanCode = (text) => {
    this.setState({ password: text })
  }


  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, paddingTop: 0 }}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
      <View>
        <ScrollView>
          <SearchBar
            style={{ width: 600 }}
            round
            searchIcon={{ size: 24 }}
            onChangeText={text => this.SearchFilterFunction(text)}
            onClear={text => this.SearchFilterFunction('')}
            placeholder="Type Here..."
            value={this.state.search}
          />

          <View style={styles.medicinelisting}>
            <Text style={{ position: "absolute", left: 20,fontSize:16,fontWeight:'500'}}>Medicine1</Text>
            <View style={{ position: "absolute", left: 160 }}><Icon name='add-circle' /></View>
            <Text style={{ position: "absolute", left: 200 }}>5</Text>
            <View style={{ position: "absolute", right: 150 }}><Icon name="remove-circle" /></View>
            <Text style={{ position: "absolute", right: 20,fontSize:16,fontWeight:'500' }} >₹100/-</Text>
          </View>
          <View style={styles.medicinelisting}>
            <Text style={{ position: "absolute", left: 20,fontSize:16,fontWeight:'500' }}>Medicine2</Text>
            <View style={{ position: "absolute", left: 160 }}><Icon name='add-circle' /></View>
            <Text style={{ position: "absolute", left: 200 }}>5</Text>
            <View style={{ position: "absolute", right: 150 }}><Icon name="remove-circle" /></View>
            <Text style={{ position: "absolute", right: 20,fontSize:16,fontWeight:'500' }} >₹100/-</Text>
          </View>
          <View style={styles.medicinelisting}>
            <Text style={{ position: "absolute", left: 20, backgroundColor: 'blue', width: 100, height: 20, paddingLeft: 20, paddingTop: 20, borderRadius: 20 }}></Text>
            <Text style={{ position: "absolute", left: 140, color: 'green',fontSize:16,fontWeight:'500' }}>90% Off</Text>
            <Text style={{ position: "absolute", right: 70, color: 'green',fontSize:16,fontWeight:'500' }}>-30₹</Text>
            <View style={{ position: "absolute", right: 10, paddingRight: 20 }} ><Icon name='close' /></View>
            <TextInput style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Coupan Code"
              placeholderTextColor="#000"
              onChangeText={this.handleCoupanCode} />
            <View style={{ width: 100, position: "absolute", right: 20, height: 20, top: 30 }}>
              <Button title="APPLY" />
            </View>
          </View>
          <View style={styles.medicinelisting}>
            <View>
              <Text style={{fontSize:16,fontWeight:'500',fontSize:16,fontWeight:'500'}}>Total Amount(MRP)</Text>
              <Text style={{ position: "absolute", right: 0,fontSize:16,fontWeight:'500' }}>₹330</Text>
            </View>
            <View>
              <Text style={{fontSize:16,fontWeight:'500'}}>Coupan Discount</Text>
              <Text style={{ position: "absolute", right: 0, color: 'green' }}>₹30</Text></View>
            <View>
              <Text style={{fontSize:16,fontWeight:'500',fontSize:16,fontWeight:'500'}}>Delivery Charge</Text>
              <Text style={{ position: "absolute", right: 0,fontSize:16,fontWeight:'500' }}>₹20</Text></View>
          </View>
          <View style={{ marginRight: 20, marginLeft: 10, marginTop: 30 }}>
            <Text style={{ borderBottomWidth: 1, borderColor: 'black',fontSize:16,fontWeight:'500' }}>Add Instruction For Order</Text>
          </View>

          <View style={styles.medicinelisting}>
            <View style={{position: "absolute",left:20}} ><Image source={require('./img/7.png')} style={{width:50,height:50}} /></View>
            <Text style={{ position: "absolute", right: 100,fontSize:15,fontWeight:'500',paddingTop:10 }}>1 Prescription Uploaded </Text>
            <View style={{ position: "absolute", right: 10, paddingRight: 20 }} ><Icon name='close' /></View>
          </View>

          <View style={{ marginTop: 20, padding: 10, backgroundColor: 'red' }}>
             <CheckBox
              label='checkbox'
              checked={this.state.checked}
            />
            <View style={{ position: "absolute", left: 80,width:60,paddingTop:10 }}>
              <Image source={require('./img/13.png')} style={{height:40,width:40}} /></View>
            <Text style={{ position: "absolute", right: 100, color: '#fff',fontWeight:'bold',fontSize:20,paddingTop:10 }}>Express Delivery</Text>
            <View style={{ position: "absolute", right: 10, paddingRight: 20,paddingTop:10 }} ><Icon name='close' /></View>
          </View>

          <View style={styles.medicinelisting}>
            <Text style={{fontSize:16,fontWeight:'500'}}>Total Amount</Text>
            <Text style={{fontSize:16,fontWeight:'500'}}>₹--</Text>
            <Text style={{color:'green',fontSize:16,fontWeight:'500'}}>you save ₹30 </Text>
            <View style={{position:"absolute", right: 0,top:30,width:100}}>
            <Button
              title="NEXT"
              onPress={() =>
                this.props.navigation.navigate('Confirmbooking')
              }
            />
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewStyle: {
    justifyContent: 'center',
    flex: 1,
    backgroundColor: 'gray',
    marginTop: Platform.OS == 'ios' ? 0 : 0
  },
  textStyle: {
    padding: 0,
  },
  medicinelisting: {
    padding: 25, backgroundColor: '#fff', marginTop: 10, marginBottom: 0, marginRight: 10, marginLeft: 10
  },
  CoupanCode: {
    paddingLeft: 20, paddingRight: 20, backgroundColor: '#fff', marginTop: 20
  },
  NextButtonStyle: {
    height: 50,
    padding: 10,
    marginTop: 50
  },
  input: {
    borderBottomWidth: 1, borderColor: 'black'
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 30,
    margin: 2,
    borderColor: '#2a4944',
    borderWidth: 1,
    backgroundColor: '#d2f7f1'
  }
});
