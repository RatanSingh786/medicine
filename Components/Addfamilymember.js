import React from 'react';
import { Text, View, StyleSheet, TextInput, FlatList, Keyboard, Button, Image, ActivityIndicator, Platform, SafeAreaView } from 'react-native';
import { SearchBar, Header, CheckBox } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import { Icon } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import ModalDropdown from 'react-native-modal-dropdown';

export default class Addfamilymember extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isLoading: true, search: '',checked:false };
  }
  handleCoupanCode = (text) => {
    this.setState({ password: text })
  }
  
  render() {
    // let data = [{
    //   value: 'Marcos',
    // }, {
    //   value: 'Daniel',
    // }, {
    //   value: 'Peater',
    // }];
    return (
      <View>
        <ScrollView>
          <View style={styles.medicinelisting}>
            <Text style={{ fontSize: 30, fontWeight: '600'}}>Add Family Member</Text>
          </View>
          <View style={styles.medicinelisting}>
            <Text style={{ fontSize: 15, fontWeight: '400'}}>Relationship</Text>
          </View>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={styles.container}>
            <View style={{position:'relative',width:180,height:70,backgroundColor:'#ccc',marginRight:10,borderRadius: 150 / 2,overflow: "hidden",borderWidth: 3,borderColor: "blue"}}>
                <Image source={require('./img/23.png')} style={{position:'absolute',left:15,width:50,height:50,top:10}}/>
                <View style={{position:'absolute',right:10,width:100,top:20}}><Text style={{fontSize:20}}>Father</Text></View>
            </View>
            <View style={{position:'relative',width:180,height:70,backgroundColor:'#ccc',borderRadius:10,marginRight:10}}>
                <Image source={require('./img/24.png')} style={{position:'absolute',left:15,width:50,height:50,top:10}}/>
                <View style={{position:'absolute',right:10,width:100,top:20}}><Text style={{fontSize:20}}>Mother</Text></View>
            </View>
            <View style={{position:'relative',width:180,height:70,backgroundColor:'#ccc',borderRadius:10,marginRight:10}}>
                <Image source={require('./img/23.png')} style={{position:'absolute',left:15,width:50,height:50,top:10}}/>
                <View style={{position:'absolute',right:10,width:100,top:20}}><Text style={{fontSize:20}}>Father</Text></View>
            </View>
            <View style={{position:'relative',width:180,height:70,backgroundColor:'#ccc',borderRadius:10,marginRight:10}}>
                <Image source={require('./img/24.png')} style={{position:'absolute',left:15,width:50,height:50,top:10}}/>
                <View style={{position:'absolute',right:10,width:100,top:20}}><Text style={{fontSize:20}}>Mother</Text></View>
            </View>
          </ScrollView>

          <View style={styles.medicinelisting}>
            <View style={{ position: 'absolute', left: 20 }}>
              <Image source={require('./img/16.png')} style={{ width: 40, height: 40 }} /></View>
            <View style={{ position: 'absolute', right: 20, width: 250 }}>
            <ModalDropdown options={['Michele', 'Marcos']} style={{ borderBottomWidth: 1, borderColor: 'black' }}/>
            </View>
          </View>
          <View style={styles.medicinelisting}>
            <View style={{ position: 'absolute', left: 20 }}>
              <Image source={require('./img/17.png')} style={{ width: 40, height: 40 }} /></View>
            <TextInput style={{ position: 'absolute', right: 20, width: 250,borderBottomWidth: 1, borderColor: 'black' }}
              underlineColorAndroid="transparent"
              placeholder="9999999999"
              placeholderTextColor="#000"
              onChangeText={this.handleCoupanCode} />
          </View>
          <View style={styles.medicinelisting}>
            <View style={{ position: 'absolute', left: 20 }}>
              <Image source={require('./img/18.png')} style={{ width: 40, height: 40 }} /></View>
            <TextInput style={{ position: 'absolute', right: 20, width: 250,borderBottomWidth: 1, borderColor: 'black' }}
              underlineColorAndroid="transparent"
              placeholder="Flat No, Apartment,Area,Landmark"
              placeholderTextColor="#000"
              onChangeText={this.handleCoupanCode} />
          </View>
          <View style={styles.medicinelisting}>
            <TextInput
              style={{ position: 'absolute', left: 130,borderBottomWidth: 1, borderColor: 'black' }}
              underlineColorAndroid="transparent"
              placeholder="28200x"
              placeholderTextColor="#000"
              onChangeText={this.handleCoupanCode} />
            <TextInput
              style={{ position: 'absolute', right: 20,borderBottomWidth: 1, borderColor: 'black' }}
              underlineColorAndroid="transparent"
              placeholder="Agra Uttar Pradesh"
              placeholderTextColor="#000"
              onChangeText={this.handleCoupanCode} />
          </View>
          <View style={styles.medicinelisting}>
            <View style={{ position: 'absolute', left: 20,top:20 }}>
              <Text>Choose Time Slot</Text>
            </View>
            <TextInput style={{ position: 'absolute', right: 20, width: 200,borderBottomWidth: 1, borderColor: 'black' }}
              underlineColorAndroid="transparent"
              placeholder="9999999999"
              placeholderTextColor="#000"
              onChangeText={this.handleCoupanCode} />
          </View>
          <View style={styles.medicinelisting}>
            <View style={{position: 'absolute', left: 0}}>
            <CheckBox
              label='checkbox'
              checked={this.state.checked}
            />
            </View>
            <View style={{position: 'absolute', right: 0}}>
            <Text>I am lawfully authorised to provide the above information on behalf of the owner of profile</Text>
            </View>
            <View style={styles.medicinelisting}>
            <Button
              title="Add Profile"
              onPress={() =>
                this.props.navigation.navigate('Ordersuccess')
              }
            />
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  medicinelisting: {
    padding: 25, marginTop: 10, marginBottom: 0, marginRight: 10, marginLeft: 10
  },
  container: {
    flex: 1,
    padding: 10,
  },
});
