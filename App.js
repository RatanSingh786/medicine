import React, { Component } from 'react';
import {SafeAreaView,StyleSheet,ScrollView,View,Text,StatusBar,} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import Home from './Components/Home';
import Confirmbooking from './Components/Confirmbooking';
import Completeorder from './Components/Completeorder';
import Addfamilymember from './Components/Addfamilymember';
import Ordersuccess from './Components/Ordersuccess';
import Orders from './Components/Orders';
import MyCart from './Components/MyCart';
import OrderHistory from './Components/OrderHistory';
import OrderHistoryDetails from './Components/OrderHistoryDetails';
import Prescription from './Components/Prescription';

const NavigationStack = createStackNavigator({
  Home: { screen: Home },
  Confirmbooking: { screen: Confirmbooking },
  Completeorder: { screen: Completeorder },
  Addfamilymember: { screen: Addfamilymember },
  Ordersuccess: { screen: Ordersuccess },
  Orders: { screen: Orders },
  MyCart: { screen: MyCart },
  OrderHistory: { screen: OrderHistory },
  OrderHistoryDetails: { screen: OrderHistoryDetails },
  Prescription : { screen: Prescription }
});

const Container = createAppContainer(NavigationStack);

class App extends Component {


  render() {
    return (
      <Container />
    )
  }
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
